#ifndef OMPILANCZ_SPAN_H
#define OMPILANCZ_SPAN_H

#include <cstddef>

namespace ompilancz
{

   // simple pre-C++20 span-like class:

   template <typename T>
   class span
   {
      public:
         span()
            : data_(nullptr), n_(0)
         { }

         span(T* data, size_t n)
            : data_(data), n_(n)
         { }

         span(T* begin, T* end)
            : data_(begin), n_(end - begin)
         { }

         // implicit conversion from any span-like class such as std::span (C++20)...
         // ...or, contiguous container classes such as std::vector, std::array, etc.
         template <typename U>
         span(U&& other_span)
            : span( other_span.data(), other_span.size() )
         { }

         T* data()
         {
            return data_;
         }
         
         const T* data() const
         {
            return data_;
         }

         size_t size() const
         {
            return n_;
         }

         T& operator[](size_t i)
         {
            return *(data_ + i);
         }
         
         const T& operator[](size_t i) const
         {
            return *(data_ + i);
         }

         T* begin()
         {
            return data_;
         }

         const T* begin() const
         { 
            return data_; 
         }

         T* end()
         {
            return data_ + n_;
         }

         const T* end() const
         {
            return data_ + n_;
         }

      private:
         T* data_;
         size_t n_;
   };

}  // namespace ompilancz

#endif
