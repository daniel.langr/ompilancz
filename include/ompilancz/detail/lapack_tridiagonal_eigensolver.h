#ifndef OMPILANCZ_DETAIL_LAPACK_TRIDIAGONAL_EIGENSOLVER_H
#define OMPILANCZ_DETAIL_LAPACK_TRIDIAGONAL_EIGENSOLVER_H

#include <cassert>
#include <cmath>
#include <stdexcept>
#include <vector>

#include "lapack_lamch.h"
#include "../span.h"

// LAPACK routines interface:
extern "C"
{
   extern void sstevx_(char*, char*, int*, float*, float*, float*, float*, int*, int*,
         float*, int*, float*, float*, int*, float*, int*, int*, int*);
   extern void dstevx_(char*, char*, int*, double*, double*, double*, double*, int*, int*,
         double*, int*, double*, double*, int*, double*, int*, int*, int*);
}

// overloaded LAPACK wrappers:
inline void stevx(char JOBZ, char RANGE, int N, float* D, float* E, float VL, float VU,
   int IL, int IU, float ABSTOL, int& M, float* W, float* Z, int LDZ, float* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   sstevx_(&JOBZ, &RANGE, &N, D, E, &VL, &VU, &IL, &IU, &ABSTOL, &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

inline void stevx(char JOBZ, char RANGE, int N, double* D, double* E, double VL, double VU,
   int IL, int IU, double ABSTOL, int& M, double* W, double* Z, int LDZ, double* WORK, int* IWORK, int* IFAIL, int& INFO)
{
   dstevx_(&JOBZ, &RANGE, &N, D, E, &VL, &VU, &IL, &IU, &ABSTOL, &M, W, Z, &LDZ, WORK, IWORK, IFAIL, &INFO);
}

namespace ompilancz
{

// Lapack symmetric tridiagonal eigensolver wrapper:

template <typename T>
class tridiagonal_eigensolver
{
   public:
      tridiagonal_eigensolver(int n_est = 0)  // n_est - estimated maximum matrix size
         : n_(0), nev_(0)
      {
         // avoid unnecessary reallocations:

         alpha_.reserve(n_est);
         beta_.reserve(n_est);

         D_.reserve(n_est);
         E_.reserve(n_est);

         W_.reserve(n_est);
         Z_.reserve(n_est * n_est);

         WORK_.reserve(5 * n_est);
         IWORK_.reserve(5 * n_est);
         IFAIL_.reserve(n_est);
      }

      void append_alphabeta(T alpha, T beta)
      {
         alpha_.push_back(alpha);
         beta_.push_back(beta);
         n_++;
      }

      void solve(int nev)  // nev - number of required eigenpairs
      {
         nev_ = nev;

         D_ = alpha_;
         E_ = beta_;

         W_.resize(n_);
         Z_.resize(n_ * nev);
         WORK_.resize(5 * n_);
         IWORK_.resize(5 * n_);
         IFAIL_.resize(n_);

         T ABSTOL = Lamch<T>::lamch('S');
         int M, INFO;

         stevx('V', 'I', n_, D_.data(), E_.data(), (T)0.0, (T)0.0, 1, std::min(n_, nev), ABSTOL, M,
               W_.data(), Z_.data(), n_, WORK_.data(), IWORK_.data(), IFAIL_.data(), INFO);

         if (INFO)
         {
//          std::cerr << "Tridiagonal eigensolver failed; ?stevx INFO not null: " << INFO << std::endl;
            throw std::runtime_error("LAPACK ?stevx failed");
         }
      }

      // eigenvalue array:
      span<T> Lambda() 
      {
         return span<T>(W_.data(), nev_);
      }

      // i-th (0-indexed) eigenvalue:
      T lambda(int i) 
      {
         assert(i < nev_);

         return W_[i];
      }

      // eigenvectors (2-D column-major) array:
      span<T> X() 
      {
         return span<T>(Z_.data(), Z_.size());
      }

      // i-th (0-indexed) eigenvector:
      span<T> x(int i) 
      {
         assert(i < nev_);

         T* begin = Z_.data() + i * n_;
         return span<T>(begin, begin + n_);
      }

      // Lanczos residual for i-th eigenpair...
      // ... calculated as abs(last beta * last element of i-th eigenvector)
      T res(int i)
      {
         assert (i < nev_);

         T last_beta = beta_.back();
         return fabs(last_beta * x(i)[n_ - 1]);
      }

   private:
      int n_;    // number of rows/columns
      int nev_;  // number of required eigenpairs

      std::vector<T> alpha_;  // main diagonal elements
      std::vector<T> beta_;   // subdiagonal elements

      std::vector<T> D_, E_;  // Lapack routine input arrays
      std::vector<T> W_;      // Lapack routine resulting eigenvalues
      std::vector<T> Z_;      // Lapack routine resulting eigenvectors

      std::vector<T> WORK_;     // Lapack routine auxiliary space
      std::vector<int> IWORK_;  // Lapack routine auxiliary space

      std::vector<int> IFAIL_;  // Lapack routine failed-to-coverge indexes
};

}  // namespace ompilancz

#endif
