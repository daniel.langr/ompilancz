#ifndef OMPILANCZ_DETAIL_MATRIX_TIMES_VECTOR_H
#define OMPILANCZ_DETAIL_MATRIX_TIMES_VECTOR_H

#include <algorithm>
#include <cassert>
#include <vector>

#include <mpi.h>

#include "diagonal_vector.h"
#include "mapping.h"
#include "mpi_datatype.h"
#include "../span.h"

namespace ompilancz
{

// matrix-times-(diagonal )vector wrapper:

template <typename T>
class matrix_times_vector
{
   public:
      matrix_times_vector(mapping& map)
         : map_(map), mdt_( mpi_datatype<T>::get() )
      {
         if (!map_.diag())
         {
            // resize auxiliary arrays for off-diagonal processes:
            offdiag_x_.resize(map_.n());
            offdiag_y_.resize(map_.m());

            offdiag_xt_.resize(map_.m());
            offdiag_yt_.resize(map_.n());

            x_ = span<T>( offdiag_x_.data(), offdiag_x_.size() );
            y_ = span<T>( offdiag_y_.data(), offdiag_y_.size() );

            xt_ = span<T>( offdiag_xt_.data(), offdiag_xt_.size() );
            yt_ = span<T>( offdiag_yt_.data(), offdiag_yt_.size() );
         }
      }

      void diag_setup_xy( diagonal_vector<T>& x, diagonal_vector<T>& y)
      {
         assert( map_.diag() );

         assert( x.size() == map_.n() );
         assert( y.size() == map_.m() );

         x_ = x.data();
         y_ = y.data();

         xt_ = x_;
         yt_ = y_;
      }

      void broadcast_x()
      {
         MPI_Bcast(x_.data(), map_.n(), mdt_, 0, map_.vcomm());
         MPI_Bcast(xt_.data(), map_.m(), mdt_, 0, map_.hcomm());
      }

      template <typename U>
      void multiply(U&& matvec_operator)
      {
         std::fill(y_.begin(), y_.end(), (T)0.0);
         if (!map_.diag())
            std::fill(yt_.begin(), yt_.end(), (T)0.0);

         matvec_operator(x_, y_, xt_, yt_);
      }

      void reduce_y()
      {
         void* sendbuf = map_.diag() ? MPI_IN_PLACE : y_.data();
         void* recvbuf = map_.diag() ? y_.data() : nullptr;
         MPI_Reduce(sendbuf, recvbuf, map_.m(), mdt_, MPI_SUM, 0, map_.hcomm());

         sendbuf = map_.diag() ? MPI_IN_PLACE : yt_.data();
         recvbuf = map_.diag() ? yt_.data() : nullptr;
         MPI_Reduce(sendbuf, recvbuf, map_.n(), mdt_, MPI_SUM, 0, map_.vcomm());
      }

   private:
      mapping& map_;
      MPI_Datatype mdt_;

      span<T> x_, y_;
      span<T> xt_, yt_;

      // off-diagonal processes data:
      std::vector<T> offdiag_x_, offdiag_y_;    // for y <- (U + D) * x
      std::vector<T> offdiag_xt_, offdiag_yt_;  // for y <- y + L * x
};

}  // namespace ompilancz

#endif
