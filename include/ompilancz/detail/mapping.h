#ifndef OMPILANCZ_DETAIL_MAPPING_H
#define OMPILANCZ_DETAIL_MAPPING_H

#include <cassert>
#include <cstdint>

#include <mpi.h>

namespace ompilancz
{

// upper-trianguler matrix-process mapping class:
class mapping
{
   public:
      mapping(
            int I, int J, int N,
            uint64_t m, uint64_t n,
            MPI_Comm allcomm = MPI_COMM_WORLD)
         : I_(I), J_(J), N_(N), m_(m), n_(n), root_(false)
      {

         // good practice not to work on an external communicator directly:
         MPI_Comm_dup(allcomm, &allcomm_);

         // setup communicators:
         MPI_Comm_split(allcomm_, I, J, &hcomm_);
         MPI_Comm_split(allcomm_, J, J - I, &vcomm_);
         MPI_Comm_split(allcomm_, I == J, I, &dcomm_);

         diag_ = I == J;

         // first diagonal process is root:
         if (diag_)
         {
            int rank;
            MPI_Comm_rank(dcomm_, &rank);
            root_ = (rank == 0);

            // diagonal blocks must be square:
            assert(m_ == n_);
         }
      }

      ~mapping() 
      {
         MPI_Comm_free(&dcomm_);
         MPI_Comm_free(&vcomm_);
         MPI_Comm_free(&hcomm_);
         MPI_Comm_free(&allcomm_);
      }

      uint64_t m() const { return m_; }
      uint64_t n() const { return n_; }

      bool diag() const { return diag_; }
      bool root() const { return root_; }

      // upper-right "corner" process:
      bool ur() const { return I_ == 0 && J_ == N_ - 1; }

      MPI_Comm& allcomm() { return allcomm_; }
      MPI_Comm& hcomm()   { return hcomm_; }
      MPI_Comm& vcomm()   { return vcomm_; }
      MPI_Comm& dcomm()   { return dcomm_; }

   private:
      int I_, J_;  // block coordinates
      int N_;      // number of diagonal blocks

      uint64_t m_, n_;  // block number of matrix rows/columns

      MPI_Comm allcomm_;  // all processes
      MPI_Comm hcomm_;    // processes in the same row
      MPI_Comm vcomm_;    // processes in the same column
      MPI_Comm dcomm_;    // diagonal (and off-diagonal) processes

      bool diag_;  // diagonal process flag (I_ == J_ is true)
      bool root_;  // root process flag (diagonal process with lowest I_)
};

}  // namespace ompilancz

#endif
