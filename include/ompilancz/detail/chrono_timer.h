#ifndef OMPILANCZ_DETAIL_CHRONO_TIMER_H
#define OMPILANCZ_DETAIL_CHRONO_TIMER_H

#include <chrono>
#include <cstdint>

namespace ompilancz
{

   // simple std::chrono based timer:

class chrono_timer
{
   public:
      enum start_options { start_now, do_not_start_now };

      explicit chrono_timer(start_options should_start = do_not_start_now) 
      {
         if (should_start == start_now)
            start();
      }

      void start()
      {
         start_ = clock_type::now();
      }

      uint64_t stop()
      {
         auto diff = clock_type::now() - start_;
         auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(diff);
         nsec_ = duration.count();
         return nsec_;
      }

      uint64_t nanoseconds() const { return nsec_; }

      double seconds() const { return double(nsec_) / 1.0e9; }
      double miliseconds() const { return double(nsec_) / 1.0e6; }
      double microseconds() const { return double(nsec_) / 1.0e3; }

   private:
      using clock_type = std::chrono::high_resolution_clock;

      std::chrono::time_point<clock_type> start_;
      uint64_t nsec_;
};

}  // namespace ompilancz

#endif
