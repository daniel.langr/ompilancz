#ifndef OMPILANCZ_DETAIL_DIAGONAL_VECTOR_H
#define OMPILANCZ_DETAIL_DIAGONAL_VECTOR_H

#include <algorithm>
#include <cassert>
#include <cstdint>
#include <limits.h>
#include <random>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include <mpi.h>

#include "mpi_datatype.h"
#include "mapping.h"
#include "../span.h"

namespace ompilancz
{

// vector split across diagonal processes:

template <typename T>
class diagonal_vector
{
   public:
      diagonal_vector(mapping& map)
         : map_(map), n_( map.m() ), data_( map.m() ), mdt_( mpi_datatype<T>::get() )
      {
         assert( map.diag() );          // diagonal vectors are usable on diagonal processors only
         assert( map.n() == map.m() );  // diagonal blocks must be square
      }

      // copy semantics:

      diagonal_vector(const diagonal_vector&) = default;

      diagonal_vector& operator=(const diagonal_vector& other)
      {
         if (&other != this)
         {
            assert( n_ == other.n_ );
            assert( &map_ == &other.map_ );  // both vectors must operate on same mapping object

            data_ = other.data_;
         }

         return *this;
      }

      // move semantics:

      diagonal_vector(diagonal_vector&& other) noexcept
         : map_(other.map_), n_(other.n_), data_(std::move(other.data_)), mdt_(other.mdt_)
      { }

      diagonal_vector& operator=(diagonal_vector&& other) noexcept
      {
         if (&other != this)
         {
            assert( n_ == other.n_ );
            assert( &map_ == &other.map_ );  // both vectors must operate on same mapping object

            data_ = std::move(other.data_);
         }

         return *this;
      }

      // size
      uint64_t size() const
      {
         assert( n_ == data_.size() );
         return n_;
      }

      // element accessors:

      T& operator[](uint64_t i)
      {
         return data_[i];
      }

      const T& operator[](uint64_t i) const
      {
         return data_[i];
      }
      
      span<T> data()
      {
         assert( n_ == data_.size() );
         return span<T>(data_.data(), n_);
      }

      const span<T> data() const
      {
         assert( n_ == data_.size() );
         return span<T>(data_.data(), n_);
      }

      // construction:

      void random(T a = (T)0.0, T b = (T)1.0)
      {
         std::random_device rd;
         std::mt19937 eng(rd());
         std::uniform_real_distribution<T> dist(a, b);

         for (auto & e : data_)
            e = dist(eng);
      }

      // mathematical operations:

      T dot_product(const diagonal_vector& other) const
      {
         assert( n_ == data_.size() );
         assert( other.n_ == other.data_.size() );
         assert( data_.size() == other.data_.size() );

         T local_res = (T)0.0;

         for (uint64_t i = 0; i < n_; i++)
            local_res += data_[i] * other.data_[i];

         T res;
         MPI_Allreduce(&local_res, &res, 1, mdt_, MPI_SUM, map_.dcomm());

         return res;
      }

      T norm() const
      {
         T dot = dot_product(*this);
         return sqrt(dot);
      }

      T normalize()
      {
         assert( n_ == data_.size() );

         T temp = norm();
         T mult = (T)1.0 / temp;

         for (uint64_t i = 0; i < n_; i++)
            data_[i] *= mult;

         return temp;
      }

      T orthogonalize(const diagonal_vector& other) 
      {
         assert( n_ == data_.size() );
         assert( other.n_ == other.data_.size() );
         assert( data_.size() == other.data_.size() );

         T dot = dot_product(other);

         for (uint64_t i = 0; i < n_; i++)
            data_[i] -= dot * other.data_[i];

         return dot;
      }

      void scale(T alpha)
      {
         assert( n_ == data_.size() );

         for (int i = 0; i < n_; i++)
            data_[i] *= alpha;
      }

      void add(const diagonal_vector& other)
      {
         assert( n_ == data_.size() );

         for (int i = 0; i < n_; i++)
            data_[i] += other.data_[i];
      }

      void subtract(const diagonal_vector& other)
      {
         assert( n_ == data_.size() );

         for (int i = 0; i < n_; i++)
            data_[i] -= other.data_[i];
      }

      void scaled_add(T alpha, const diagonal_vector& other)
      {
         assert( n_ == data_.size() );

         for (int i = 0; i < n_; i++)
            data_[i] += alpha * other.data_[i];
      }

      void scaled_subtract(T alpha, const diagonal_vector& other)
      {
         assert( n_ == data_.size() );

         for (int i = 0; i < n_; i++)
            data_[i] -= alpha * other.data_[i];
      }

      template <typename U = T>
      void store_to_file(const std::string& filename)
      {
         U* data;
         MPI_Datatype mdt;
         std::vector<U> temp;

         if (std::is_same<T, U>::value)
         {
            data = (U*)data_.data();
            mdt = mdt_;
         }
         else 
         {
            temp.assign(data_.begin(), data_.end());
            data = temp.data();
            mdt = mpi_datatype<U>::get();
         }

         uint64_t offset;
         MPI_Scan(&n_, &offset, 1, MPI_UINT64_T, MPI_SUM, map_.dcomm());
         offset -= n_;
         offset *= sizeof(U);  // offset required in bytes

         // try to delete file first:
         if (map_.root())
            MPI_File_delete(filename.c_str(), MPI_INFO_NULL);  // ignore errors
         MPI_Barrier(map_.dcomm());

         MPI_File f;
         MPI_File_open(map_.dcomm(), filename.c_str(),
               MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &f);
         MPI_File_write_at(f, offset, data, n_, mdt, MPI_STATUS_IGNORE);
         MPI_File_close(&f);
      }

      template <typename U = T>
      void load_from_file(const std::string& filename)
      {
         U* data;
         MPI_Datatype mdt;
         std::vector<U> temp;

         if (std::is_same<T, U>::value)
         {
            data = (U*)data_.data();
            mdt = mdt_;
         }
         else 
         {
            temp.resize(n_);
            data = temp.data();
            mdt = mpi_datatype<U>::get();
         }

         uint64_t offset;
         MPI_Scan(&n_, &offset, 1, MPI_UINT64_T, MPI_SUM, map_.dcomm());
         offset -= n_;
         offset *= sizeof(U);  // offset required in bytes

         MPI_File f;
         MPI_File_open(map_.dcomm(), filename.c_str(), MPI_MODE_RDONLY, MPI_INFO_NULL, &f);
         MPI_File_read_at(f, offset, data, n_, mdt, MPI_STATUS_IGNORE);
         MPI_File_close(&f);

         if (!std::is_same<T, U>::value)
            std::copy(temp.begin(), temp.end(), data_.begin());
      }

   private:
      mapping& map_;         // matrix-processes mapping
      const uint64_t n_;     // process-local number of elements 
      std::vector<T> data_;  // process-local vector elements
      MPI_Datatype mdt_;     // MPI_Datatype for T
};

}  // namespace ompilancz

#endif
