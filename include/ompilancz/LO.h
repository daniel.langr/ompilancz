#ifndef OMPILANCZ_LO_H
#define OMPILANCZ_LO_H

#include <mpi.h>

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "span.h"
#include "detail/block_diagonal_vector.h"
#include "detail/mapping.h"
#include "detail/mpi_datatype.h"

namespace ompilancz
{

// Lanczos overlaps:

template <typename T>
class lo
{
   public:
      lo(
            int I, int J, int N,    // process coordinates, number of diagonal processes
            uint64_t n, uint64_t m  // process-local matrix block dimensions
         )
         : map_(I, J, N, n, m), mdt_( mpi_datatype<T>::get() )
      {
         if (map_.diag())
            p_ = std::make_unique< block_diagonal_vector<T> >(map_);
      }

      template <typename U>
      void load_pivots(const std::vector<std::string>& pivot_filenames)
      {
         if (map_.root())
            std::cout << "Reading pivots:" << std::endl;

         if (map_.diag())
         {
            for (const auto& pivot_filename : pivot_filenames)
            {
               if (map_.root())
                  std::cout << "   from file : " << pivot_filename << "...";

               p_->template add_from_file<U>(pivot_filename);

               if (map_.root())
                  std::cout << " [done]" << std::endl;
            }
         }

         if (map_.root())
            std::cout << std::endl;
      }

      template <typename U>
      void multiply(const block_diagonal_vector<U>& lanczos_vectors)
      {
         if (map_.root())
            overlaps_.resize(p_->size());

         if (map_.diag())
         {
            for (int ip = 0; ip < p_->size(); ip++)
            {
               if (map_.root())
                  overlaps_[ip].resize(lanczos_vectors.size());

               for (int ilv = 0; ilv < lanczos_vectors.size(); ilv++)
               {
                  auto res = p_->ith_vector(ip).dot_product(lanczos_vectors.ith_vector(ilv));
                  
                  if (map_.root())
                     overlaps_[ip][ilv] = res;
               }
            }
         }
      }

      T result(int ip, int ilv)
      {
         assert (map_.root());
         return overlaps_[ip][ilv];
      }

   private:
      mapping map_;
      MPI_Datatype mdt_;
      std::unique_ptr< block_diagonal_vector<T> > p_;
      std::vector<std::vector<T>> overlaps_;
};

}  // namespace ompilancz

#endif
