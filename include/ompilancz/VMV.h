#ifndef OMPILANCZ_VMV_H
#define OMPILANCZ_VMV_H

#include <mpi.h>

#include <memory>

#include "span.h"
#include "detail/block_diagonal_vector.h"
#include "detail/mapping.h"
#include "detail/matrix_times_vector.h"
#include "detail/mpi_datatype.h"

namespace ompilancz
{

// vector-matrix-vector multiplication:

template <typename T>
class vmv
{
   public:
      vmv(
            int I, int J, int N,    // process coordinates, number of diagonal processes
            uint64_t n, uint64_t m  // process-local matrix block dimensions
         )
         : map_(I, J, N, n, m), mdt_( mpi_datatype<T>::get() )
      {
         if (map_.diag())
         {
            v_ = std::make_unique< block_diagonal_vector<T> >(map_);
            w_ = std::make_unique< block_diagonal_vector<T> >(map_);
         }
      }

      template <typename U>
      void add_vector(span<U> values)
      {
         v_->add_span(values);
         w_->add_zero();
      }

      template <typename U>
      void multiply(
            U&& matvec_operator)  // process-local matrix block x vector operator
      {
         // setup matrix-vector multiplication wrapper:
         matrix_times_vector<T> matvec(map_);

         // broadcast nwfn to off-diagonal processes:
         int n = map_.diag() ? v_->size() : 0;
         MPI_Bcast(&n, 1, MPI_INT, 0, map_.allcomm());

         // matrix-vector multiplication:
         for (int i = 0; i < n; i++)
         {
            if (map_.diag())
               matvec.diag_setup_xy(v_->ith_vector(i), w_->ith_vector(i));

            matvec.broadcast_x();
            matvec.multiply(matvec_operator);
            matvec.reduce_y();
         }
      }

      T result(int i, int j)
      {
         return v_->ith_vector(i).dot_product(w_->ith_vector(j));
      }

   private:
      mapping map_;
      MPI_Datatype mdt_;
      std::unique_ptr< block_diagonal_vector<T> > v_;
      std::unique_ptr< block_diagonal_vector<T> > w_;
};

}  // namespace ompilancz

#endif
