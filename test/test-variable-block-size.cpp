#include <cassert>
#include <cstdlib>
#include <iostream>
#include <tuple>

#include <mpi.h>

#include <ompilancz/eigensolver.h>
#include <ompilancz/span.h>

class matvec_operator
{
   public:
      matvec_operator(int I, int J, int m, int n, int base) : I_(I), J_(J), m_(m), n_(n)
      {
         first_ = (I * (I + 1) / 2) * base;
      }

      void operator()(
            ompilancz::span<double> x, ompilancz::span<double> y,
            ompilancz::span<double> xt, ompilancz::span<double> yt
            ) const
      {

         if (I_ == J_)
         {
            // diagonal process:
            for (int i = 0; i < m_; i++)
               for (int j = i; j < n_; j++)
               {
                  if (i == j)
                     // diagonal elements:
                     y[i] += (double)(first_ + i + 1) * x[j];
                  else
                  {
                     // off-diagonal elements:
                     y[i] += -1.0 * x[j];
                     yt[j] += -1.0 * xt[i];
                  }
               }
         }
         else
         {
            // off-diagonal process:
            for (int i = 0; i < m_; i++)
               for (int j = 0; j < n_; j++)
               {
                  y[i] += -1.0 * x[j];
                  yt[j] += -1.0 * xt[i];
               }
         }
      }

   private:
      int I_, J_;
      int m_, n_;
      int first_;
};

std::tuple<int, int, int> get_IJN(int, int);

int main(int argc, char* argv[])
{
   MPI_Init(&argc, &argv);

   int rank, nprocs;
   MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

   // process coordinates:

   int I, J, N;
   std::tie(I, J, N) = get_IJN(rank, nprocs);

   // matrix:

   uint64_t base;
   if (argc < 2)
      base = 100;
   else
      base = atol(argv[1]);

   uint64_t m = (I + 1) * base;
   uint64_t n = (J + 1) * base;

   // wrapping in a block to clean up communicaotrs in destructors before MPI_Finalize:
   {
      matvec_operator mv(I, J, m, n, base);
      ompilancz::eigensolver<double> es(I, J, N, m, n);
      es.solve(mv, 10, 1000, 1.0e-6);
   }

   MPI_Finalize();
}

std::tuple<int, int, int>get_IJN(int rank, int nprocs)
{
   // get nubmer of diagonal processes N: 

   int N = 1;
   while (true)
   {
      int temp = N * (N + 1) / 2;

      if (temp == nprocs)
         break;

      if (temp > nprocs)
      {
         if (rank == 0)
            std::cerr << "Incorrect number of MPI processes; must be N*(N+1)/2 for some i." << std::endl;

         MPI_Abort(MPI_COMM_WORLD, -1);
      }

      N++;
   }

   // get process coordinates (I, J):

   int I = 0;
   int J = 0;

   int r = 0;
   for ( ; r < nprocs; r++)
   {
      if (r == rank)
         break;

      I++;
      J++;

      if (J >= N)
      {
         J = N - I + 1;
         I = 0;
      }
   }

   assert(r == rank);
   assert(r < nprocs);

   return { I, J, N };
}
